﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretManager : MonoBehaviour
{

    // Object the turrets are aiming at
    [SerializeField]
    Transform target;

    // First turret
    [SerializeField]
    Transform turretOne;
    [SerializeField]
    Transform turretOneBarrelRotation;

    // Second turret
    [SerializeField]
    Transform turretTwo;
    [SerializeField]
    Transform turretTwoBarrelRotation;


    void Update()
    {
        // Every update, we need to calculate which turret should aim at the target, and display that in the console
        // Todo this, we're going to use the dot product to determine which turret has a better angle to the target
        // A dot B = |A|*|B|*cos(angle) = (Ax * Bx) + (Ay * By)
        // Solve the above for the angle

        // NOTE: Do not use any of Unity's built in functions to compute this
        // You may use the trig functions in the Mathf library (such as Mathf.Acos)

        // Compute the angle between turret one and the target

        double turretOneDelta = CalculateDelta(turretOneBarrelRotation, target);

        // Compute the angle between turret two and the target

        double turretTwoDelta = CalculateDelta(turretTwoBarrelRotation, target);

        // Finally, using the angles, determine which turret should currently aim at the target (whichever has a smaller rotation)
        // and print either "Turret 1" or "Turret 2" to the console

        if (turretOneDelta < turretTwoDelta)
        {
            Debug.Log("Turret One Wins");
        }
        else if (turretTwoDelta < turretOneDelta)
        {
            Debug.Log("Turret Two Wins");
        }
        else
        {
            Debug.Log("Neither Turret Wins");
        }
    }

    double CalculateDelta(Transform turret, Transform target)
    {
        double turretAngle = turret.rotation.eulerAngles.z;
        Vector3 targetVector = target.position - turret.parent.position;
        targetVector.Normalize();
        double targetAngle = Mathf.Atan(targetVector.y / targetVector.x) * Mathf.Rad2Deg;
        double angleDelta = targetAngle - turretAngle;
        if(angleDelta >= 0)
        {
            return angleDelta;
        }
        else
        {
            return angleDelta * -1;
        }
    }
}
