﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ripple : MonoBehaviour
{
    [SerializeField] float rippleRate = 3.0f;
    [SerializeField] float rippleRadius = 100.0f;
    [SerializeField] float bumpStrength = 400.0f;
    [SerializeField] float rippleResolution = 10.0f;

    float timeSinceRipple = 0;

    // Update is called once per frame
    void Update()
    {
        timeSinceRipple += Time.deltaTime;
        if (timeSinceRipple > rippleRate)
        {
            StartCoroutine(StartRipple(rippleRate, rippleRadius, bumpStrength, rippleResolution));
            timeSinceRipple -= rippleRate;
        }
    }

    void Bump(Collider hit)
    {
        try
        {
            hit.attachedRigidbody.AddForceAtPosition(Vector3.up * bumpStrength, new Vector3(
            hit.transform.position.x + 0.5f,
            hit.transform.position.y - 0.5f,
            hit.transform.position.z));
        }
        catch (NullReferenceException)
        {
            Debug.Log(hit.name + " probably doesn't have a rigidbody");
        }
    }

    IEnumerator StartRipple(float rate, float radius, float strength, float resolution)
    {
        float tickRate = rate / resolution;
        float tickDistance = radius / resolution;
        //Collider[] hits = Physics.OverlapSphere(Vector3.zero, radius);
        float elapsedTime = 0;
        float elapsedRadius = 0;
        while (elapsedTime < rate)
        {
            foreach (var hit in Physics.OverlapSphere(Vector3.zero, radius))
            {
                float hitDistance = Vector3.Distance(Vector3.zero, hit.transform.position);
                if (hitDistance > elapsedRadius && hitDistance < elapsedRadius + tickDistance)
                {
                    Bump(hit);
                }
            }
            elapsedTime += tickRate;
            elapsedRadius += tickDistance;
            yield return new WaitForSeconds(tickRate);
        }
    }
}
