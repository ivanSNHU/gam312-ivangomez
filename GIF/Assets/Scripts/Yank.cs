﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Yank : MonoBehaviour
{
    [SerializeField] float yankForce = 10.0f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {            
            foreach(var rayHit in Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition)))
            {
                if(rayHit.collider.tag == "Terrain")
                {
                    foreach (var hit in Physics.OverlapSphere(rayHit.point, 5.0f))
                    {
                        try
                        {
                            Vector3 delta = hit.transform.position - rayHit.point;
                            hit.attachedRigidbody.AddForceAtPosition(delta.normalized * yankForce, rayHit.point);
                        }
                        catch (NullReferenceException)
                        {
                            if (hit.tag == "Terrain")
                            {
                                //do nothing
                            }
                            else
                            {
                                Debug.Log(hit.name + " probably has no rigidbody");
                            }
                        }
                    }
                }
            }
        }
    }
}
