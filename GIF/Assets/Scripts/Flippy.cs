﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flippy : MonoBehaviour
{
    [SerializeField] Vector3 forceDirection = new Vector3(1, 0, 1);
    [SerializeField] float bumpRate = 2.0f;
    [SerializeField] float bumpStrength = 1.0f;
    [SerializeField] float bumpDuration = 1.0f;
    Rigidbody rb;
    float elapsedTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        elapsedTime += dt;
        if(elapsedTime < bumpDuration)
        {
            Bump(dt);
        }
        if (elapsedTime >= bumpRate)
        {
            elapsedTime -= bumpRate;
        }
    }

    private void Bump(float dt)
    {
        //Vector3 temp = rb.rotation.eulerAngles;
        //temp += forceDirection * bumpStrength * dt;
        //rb.MoveRotation(Quaternion.Euler(temp));

        rb.AddForceAtPosition(forceDirection * bumpStrength * dt, new Vector3(
            transform.position.x + 0.5f,
            transform.position.y - 0.5f,
            transform.position.z));
    }
}
