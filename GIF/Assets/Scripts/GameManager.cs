﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] Vector3 spawnPoint = new Vector3(49.5f, 0.5f, -50.0f);
    [SerializeField] GameObject cube;
    [SerializeField] float spawnRate = 3.0f;
    [SerializeField] int spawns = 25;
    [SerializeField] float spacing = 4.0f;
    float lastSpawn = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        lastSpawn = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        lastSpawn += Time.deltaTime;
        if (lastSpawn > spawnRate)
        {
            Spawn();
            lastSpawn -= spawnRate;
        }
    }

    private void Spawn()
    {
        for (int i = 0; i < spawns; i++)
        {
            Vector3 position = new Vector3(spawnPoint.x, spawnPoint.y, spawnPoint.z + (i * spacing));
            Instantiate(cube, position, Quaternion.identity);
        }
    }
}