﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slide : MonoBehaviour
{
    [SerializeField] Vector3 moveDirection = new Vector3(1.0f, 1.0f, 1.0f);
    [SerializeField] float moveSpeed = 1.0f;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        rb.MovePosition(rb.position + moveDirection * moveSpeed * dt);
    }
}
