﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public ItemDef itemDef;
    public bool pickedUp = false;
    //SpriteRenderer sr;


    // Start is called before the first frame update
    void Start()
    {
        if (itemDef != null)
        {
            CollidersSetup();
            RigidbodySetup();
            SpriteSetup();
            BoxesSetup();
        }
    }

    private void BoxesSetup()
    {
        foreach (BoxCollider box in GetComponents<BoxCollider>())
        {
            GameObject newBox = Instantiate(GameManager.instance.boxPrefab, transform);
            newBox.transform.localPosition = box.center;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpriteSetup()
    {
        SpriteRenderer sr = gameObject.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        sr.sprite = itemDef.sprite;
        sr.material = GameManager.instance.spriteMaterial;
        GameObject tempObj = new GameObject();
        tempObj.transform.parent = transform;
        tempObj.transform.localPosition = Vector3.forward * 1.005f;
        tempObj.transform.localRotation = Quaternion.Euler(0, 180, 0);
        SpriteRenderer otherSR = tempObj.AddComponent<SpriteRenderer>();
        otherSR.sprite = itemDef.sprite;
        otherSR.material = GameManager.instance.spriteMaterial;
    }

    void CollidersSetup()
    {
        for (int i = -1; i < itemDef.grid.Length; i++)
        {
            Vector2 shift = Vector2.zero;
            if (i != -1)
            {
                switch (itemDef.grid[i])
                {
                    case Helpers.Cardinals.north:
                        shift += Vector2.up;
                        break;
                    case Helpers.Cardinals.northEast:
                        shift += Vector2.up + Vector2.right;
                        break;
                    case Helpers.Cardinals.northWest:
                        shift += Vector2.up + Vector2.left;
                        break;
                    case Helpers.Cardinals.south:
                        shift += Vector2.down;
                        break;
                    case Helpers.Cardinals.southEast:
                        shift += Vector2.down + Vector2.right;
                        break;
                    case Helpers.Cardinals.southWest:
                        shift += Vector2.down + Vector2.left;
                        break;
                    case Helpers.Cardinals.east:
                        shift += Vector2.right;
                        break;
                    case Helpers.Cardinals.west:
                        shift += Vector2.left;
                        break;
                }
            }
            BoxCollider bc = gameObject.AddComponent<BoxCollider>();
            bc.center = new Vector3(shift.x, shift.y, 0.501f);
        }
    }

    void RigidbodySetup()
    {
        gameObject.AddComponent<Rigidbody>();
    }
}
