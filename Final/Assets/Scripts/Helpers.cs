﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helpers
{
    public enum Cardinals
    {
        north,
        northEast,
        northWest,
        south,
        southEast,
        southWest,
        east,
        west
    }

    public static Vector2 CardinalToVector(Cardinals c)
    {
        switch (c)
        {
            case Cardinals.north:
                return new Vector2(0, 1);
            case Cardinals.northEast:
                return new Vector2(1, 1);
            case Cardinals.northWest:
                return new Vector2(-1, 1);
            case Cardinals.south:
                return new Vector2(0, -1);
            case Cardinals.southEast:
                return new Vector2(1, -1);
            case Cardinals.southWest:
                return new Vector2(-1, -1);
            case Cardinals.east:
                return new Vector2(1, 0);
            case Cardinals.west:
                return new Vector2(-1, 0);
            default:
                return Vector2.zero;
        }
    }
}
