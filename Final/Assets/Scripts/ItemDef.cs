﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Item Def")]
public class ItemDef : ScriptableObject
{
    public Sprite sprite;
    public Helpers.Cardinals[] grid;
}
