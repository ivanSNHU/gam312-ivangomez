﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    bool canJump = false;
    [SerializeField] GameObject CameraObject;
    [SerializeField] float speed = 500.0f;
    [SerializeField] float jumpSpeed = 500.0f;
    [SerializeField] float horizontalSensitivity = 100.0f;
    [SerializeField] float verticalSensitivity = 100.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        Move(Time.fixedDeltaTime);
        HorizontalRotation(Time.fixedDeltaTime);
        VerticalRotation(Time.fixedDeltaTime);
        CheckCanJump(Time.fixedDeltaTime);
        Jump(Time.fixedDeltaTime);
        SimulateDrag(Time.fixedDeltaTime);
    }

    private void CheckCanJump(float fixedDeltaTime)
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, 0.9f, Vector3.down, out hitInfo, 1.0f))
        {
            canJump = true;
        }
        else
        {
            canJump = false;
        }
    }

    private void SimulateDrag(float fdt)
    {
        rb.velocity = new Vector3(
            rb.velocity.x * (1 - fdt),
            rb.velocity.y,
            rb.velocity.z * (1 - fdt));
    }

    void Move(float fdt)
    {
        Vector3 temp = new Vector3(
            Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical"));
        if (temp.sqrMagnitude > 1.0f)
        {
            temp.Normalize();
        }
        rb.AddRelativeForce(speed * temp * fdt);
    }

    void HorizontalRotation(float fdt)
    {
        Vector3 temp = new Vector3(
            0,
            Input.GetAxis("Mouse X") * horizontalSensitivity * fdt,
            0);
        transform.Rotate(temp, Space.World);
    }

    void VerticalRotation(float fdt)
    {
        CameraObject.transform.Rotate(
            -Input.GetAxis("Mouse Y") * verticalSensitivity * fdt,
            0, 0, Space.Self);
    }

    void Jump(float fdt)
    {
        if (Input.GetButtonDown("Jump") && canJump)
        {
            rb.AddForce(Vector3.up * jumpSpeed);
            canJump = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "PickUp")
        {
            Item temp = collision.collider.gameObject.GetComponent<Item>();
            if (!temp.pickedUp)
            {
                if (PlayerInventory.instance.AddToInventory(temp.itemDef))
                {
                    temp.pickedUp = true;
                    Destroy(collision.gameObject);
                }
            }
        }
    }
}
