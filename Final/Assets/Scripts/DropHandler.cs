﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour, IDropHandler
{
    public Vector2 position;

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Dropped " + DragHandler.draggedSlot.item.name + " at: " + position);
        if(PlayerInventory.instance.AddToInventory((int)position.x, (int)position.y, DragHandler.draggedSlot.item))
        {
            PlayerInventory.instance.RemoveFromInventory((int)DragHandler.draggedSlot.position.x, (int)DragHandler.draggedSlot.position.y);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
