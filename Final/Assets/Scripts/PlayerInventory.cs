﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    public static PlayerInventory instance;
    List<Transform> transformList = new List<Transform>();
    Dictionary<Vector2, GameObject> GUISlots = new Dictionary<Vector2, GameObject>();
    GameObject[,] GUIArray;
    InventorySlot[,] inventorySlots;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("ChildCount: " + transform.childCount);
        foreach (Transform t in transform)
        {
            transformList.Add(t);
        }
        //Debug.Log("ListTest count: " + listTest.Count);
        foreach (Transform t in transformList)
        {
            GUISlots.Add(t.position, t.gameObject);
        }
        //Debug.Log("Dictionary count: " + slots.Count);
        GUIArray = ParseSlots(GUISlots);
        Debug.Log("Slot array dimenions: " + GUIArray.GetLength(0) + ", " + GUIArray.GetLength(1));
        inventorySlots = new InventorySlot[GUIArray.GetLength(0), GUIArray.GetLength(1)];
        for (int x = 0; x < inventorySlots.GetLength(0); x++)
        {
            for (int y = 0; y < inventorySlots.GetLength(1); y++)
            {
                inventorySlots[x, y] = new InventorySlot();
                inventorySlots[x, y].position = new Vector2(x, y);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    GameObject[,] ParseSlots(Dictionary<Vector2, GameObject> slots)
    {
        List<float> xList = setToList(GetXValues(slots));
        List<float> yList = setToList(GetYValues(slots));

        GameObject[,] returnMe = new GameObject[xList.Count, yList.Count];

        for (int y = 0; y < yList.Count; y++)
        {
            for (int x = 0; x < xList.Count; x++)
            {
                Vector2 temp = new Vector2(xList[x], yList[y]);
                try
                {
                    returnMe[x, y] = slots[temp];
                    returnMe[x, y].GetComponent<DropHandler>().position = new Vector2(x, y);
                }
                catch (Exception e)
                {
                    Debug.Log(e.StackTrace);
                    Debug.Log(e.GetType().ToString());
                }
            }
        }

        return returnMe;
    }

    HashSet<float> GetYValues(Dictionary<Vector2, GameObject> slots)
    {
        HashSet<float> values = new HashSet<float>();
        foreach (var key in slots.Keys)
        {
            values.Add(key.y);
        }
        return values;
    }

    HashSet<float> GetXValues(Dictionary<Vector2, GameObject> slots)
    {
        HashSet<float> values = new HashSet<float>();
        foreach (var key in slots.Keys)
        {
            values.Add(key.x);
        }
        return values;
    }

    List<float> setToList(HashSet<float> input)
    {
        List<float> output = new List<float>();
        foreach (var i in input)
        {
            output.Add(i);
        }
        return output;
    }

    public class InventorySlot
    {
        public Vector2 position;
        public bool full;
        public ItemDef item;
        public GameObject imageDisplay;

        public InventorySlot()
        {
            position = Vector2.zero;
            full = false;
        }
    }

    public bool AddToInventory(int x, int y, ItemDef item)
    {
        if (!WithinBounds(x, y))
        {
            return false;
        }
        for (int i = -1; i < item.grid.Length; i++)
        {
            if (i == -1)
            {
                if (!IsSlotEmpty(x, y))
                {
                    return false;
                }
            }
            else
            {
                Vector2 temp = Helpers.CardinalToVector(item.grid[i]);
                temp.x += x;
                temp.y += y;
                if (!IsSlotEmpty(temp))
                {
                    return false;
                }
            }
        }
        for (int i = -1; i < item.grid.Length; i++)
        {
            if (i == -1)
            {
                inventorySlots[x, y].full = true;
            }
            else
            {
                Vector2 temp = Helpers.CardinalToVector(item.grid[i]);
                temp.x += x;
                temp.y += y;
                inventorySlots[(int)temp.x, (int)temp.y].full = true;
            }
        }
        inventorySlots[x, y].item = item;
        DrawItem(x, y);
        return true;
    }

    bool IsSlotEmpty(int x, int y)
    {
        if (!WithinBounds(x, y))
        {
            return false;
        }
        if (inventorySlots[x, y].full)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    bool IsSlotEmpty(Vector2 xy)
    {
        int x = (int)xy.x;
        int y = (int)xy.y;
        return IsSlotEmpty(x, y);
    }

    void DrawItem(int x, int y)
    {
        //GameObject temp = Instantiate(GameManager.instance.spritePrefab, transform);
        //temp.transform.localPosition = GUIArray[x, y].transform.localPosition;
        //temp.GetComponent<Image>().sprite = inventorySlots[x, y].item.sprite;
        //inventorySlots[x, y].imageDisplay = temp;
        //for (int a = 0; a < GUIArray.GetLength(0); a++)
        //{
        //    for(int b = 0; b < GUIArray.GetLength(1); b++)
        //    {
        //        if(inventorySlots[a,b].full)
        //        {
        //            GUIArray[a, b].GetComponent<Image>().color = Color.grey;
        //        }
        //    }
        //}
        for (int i = -1; i < inventorySlots[x, y].item.grid.Length; i++)
        {
            if (i == -1)
            {
                GameObject temp = Instantiate(GameManager.instance.spritePrefab, transform);
                temp.transform.localPosition = GUIArray[x, y].transform.localPosition;
                temp.GetComponent<Image>().sprite = inventorySlots[x, y].item.sprite;
                temp.GetComponent<DragHandler>().inventorySlot = inventorySlots[x, y];
                inventorySlots[x, y].imageDisplay = temp;
                GUIArray[x, y].GetComponent<Image>().color = Color.grey;
            }
            else
            {
                Vector2 tempV2 = Helpers.CardinalToVector(inventorySlots[x, y].item.grid[i]);
                tempV2.x += x;
                tempV2.y += y;
                GameObject temp = Instantiate(GameManager.instance.spritePrefab, transform);
                temp.transform.localPosition = GUIArray[(int)tempV2.x, (int)tempV2.y].transform.localPosition;
                temp.GetComponent<Image>().sprite = inventorySlots[x, y].item.sprite;
                temp.GetComponent<Image>().color = Color.gray;
                temp.GetComponent<DragHandler>().inventorySlot = inventorySlots[x, y];
                inventorySlots[(int)tempV2.x, (int)tempV2.y].imageDisplay = temp;
                GUIArray[(int)tempV2.x, (int)tempV2.y].GetComponent<Image>().color = Color.grey;
            }
        }
    }

    public bool RemoveFromInventory(int x, int y)
    {
        if (!WithinBounds(x, y))
        {
            return false;
        }
        for (int i = -1; i < inventorySlots[x, y].item.grid.Length; i++)
        {
            if (i == -1)
            {
                if (IsSlotEmpty(x, y))
                {
                    return false;
                }
            }
            else
            {
                Vector2 temp = Helpers.CardinalToVector(inventorySlots[x, y].item.grid[i]);
                temp.x += x;
                temp.y += y;
                if (IsSlotEmpty(temp))
                {
                    return false;
                }
            }
        }
        for (int i = -1; i < inventorySlots[x, y].item.grid.Length; i++)
        {
            if (i == -1)
            {
                inventorySlots[x, y].full = false;
            }
            else
            {
                Vector2 temp = Helpers.CardinalToVector(inventorySlots[x, y].item.grid[i]);
                temp.x += x;
                temp.y += y;
                inventorySlots[(int)temp.x, (int)temp.y].full = false;
            }
        }        
        UnDrawItem(x, y);
        inventorySlots[x, y].item = null;
        return true;
    }

    private void UnDrawItem(int x, int y)
    {
        //Destroy(inventorySlots[x, y].imageDisplay);
        //for (int a = 0; a < GUIArray.GetLength(0); a++)
        //{
        //    for (int b = 0; b < GUIArray.GetLength(1); b++)
        //    {
        //        if (!inventorySlots[a, b].full)
        //        {
        //            GUIArray[a, b].GetComponent<Image>().color = Color.white;
        //        }
        //    }
        //}
        for (int i = -1; i < inventorySlots[x, y].item.grid.Length; i++)
        {
            if (i == -1)
            {
                //GameObject temp = Instantiate(GameManager.instance.spritePrefab, transform);
                //temp.transform.localPosition = GUIArray[x, y].transform.localPosition;
                //temp.GetComponent<Image>().sprite = inventorySlots[x, y].item.sprite;
                //inventorySlots[x, y].imageDisplay = temp;
                //GUIArray[x, y].GetComponent<Image>().color = Color.grey;
                Destroy(DragHandler.draggedObject);
                Destroy(inventorySlots[x, y].imageDisplay);
                GUIArray[x, y].GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            }
            else
            {
                Vector2 tempV2 = Helpers.CardinalToVector(inventorySlots[x, y].item.grid[i]);
                tempV2.x += x;
                tempV2.y += y;
                //GameObject temp = Instantiate(GameManager.instance.spritePrefab, transform);
                //temp.transform.localPosition = GUIArray[(int)tempV2.x, (int)tempV2.y].transform.localPosition;
                //temp.GetComponent<Image>().sprite = inventorySlots[x, y].item.sprite;
                //temp.GetComponent<Image>().color = Color.black;
                Destroy(inventorySlots[(int)tempV2.x, (int)tempV2.y].imageDisplay);
                GUIArray[(int)tempV2.x, (int)tempV2.y].GetComponent<Image>().color = new Color(1,1,1,0.5f);
            }
        }
    }

    public bool AddToInventory(ItemDef item)
    {
        foreach (var slot in inventorySlots)
        {
            if (AddToInventory((int)slot.position.x, (int)slot.position.y, item))
            {
                return true;
            }
        }
        return false;
    }

    bool WithinBounds(int x, int y)
    {
        if (x < 0 || x >= inventorySlots.GetLength(0))
        {
            return false;
        }
        if (y < 0 || y >= inventorySlots.GetLength(1))
        {
            return false;
        }
        return true;
    }
}
