﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static PlayerInventory.InventorySlot draggedSlot;
    public PlayerInventory.InventorySlot inventorySlot;
    public static GameObject draggedObject;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (inventorySlot.imageDisplay.GetComponent<Image>().color == Color.white)
        {
            draggedSlot = inventorySlot;
            draggedObject = Instantiate(GameManager.instance.dragPrefab, transform.parent);
            draggedObject.GetComponent<Image>().sprite = inventorySlot.imageDisplay.GetComponent<Image>().sprite;
        }
        Debug.Log("BeginDrag");
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (inventorySlot.imageDisplay.GetComponent<Image>().color == Color.white)
        {
            draggedObject.transform.localPosition = new Vector3(
                Input.mousePosition.x - Screen.width / 10 - 25,// - Screen.width / 4,
                Input.mousePosition.y - Screen.height / 2,
                0);
        }
        //Debug.Log(Input.mousePosition);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Destroy(draggedObject);
        Debug.Log("EndDrag");
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    static void ClearDrag()
    {
        Destroy(draggedObject);
    }
}
