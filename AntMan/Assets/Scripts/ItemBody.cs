﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBody : MonoBehaviour
{
    public Item item;
    SpriteRenderer spriteRenderer;
    bool goingUp = true;
    [SerializeField] float minScale = 1.5f;
    [SerializeField] float maxScale = 2.0f;
    [SerializeField] float scaleSpeed = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = item.itemDef.sprite;
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;

        if (transform.localScale.magnitude > maxScale)
            goingUp = false;
        else if (transform.localScale.magnitude < minScale)
            goingUp = true;

        if (goingUp)
        {
            transform.localScale = new Vector3(
                transform.localScale.x + (scaleSpeed * dt),
                transform.localScale.y + (scaleSpeed * dt),
                transform.localScale.z + (scaleSpeed * dt));
        }
        else
        {
            transform.localScale = new Vector3(
                transform.localScale.x - (scaleSpeed * dt),
                transform.localScale.y - (scaleSpeed * dt),
                transform.localScale.z - (scaleSpeed * dt));
        }        
    }
}
