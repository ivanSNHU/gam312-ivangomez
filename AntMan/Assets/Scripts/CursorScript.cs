﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorScript : MonoBehaviour
{
    public static CursorScript instance;
    public int position, slots;
    [SerializeField] float zeroPosition, stride;
    bool held = false;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float value = Input.GetAxis("DPad_XAxis_1");
        if (value > 0.5f && !held)
        {
            position++;
            held = true;
        }
        else if (value < -0.5f && !held)
        {
            position--;
            held = true;
        }
        else if (Mathf.Abs(value) < 0.5)
        {
            held = false;
        }
        position = Mathf.Clamp(position, 0, slots - 1);
        transform.localPosition = new Vector3(
            ((stride * position) + zeroPosition),
            transform.localPosition.y,
            transform.localPosition.z);
    }
}
