﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Item Def")]
public class ItemDef : ScriptableObject
{
    public enum ItemType
    {
        Weapon,
        Armor,
        Consumable
    }
    public enum EffectType
    {
        MaxHealth,
        Damage,
        Healing,
        Repair,
        Armor
    }
    public enum EquipType
    {
        head,
        shoulders,
        knees,
        toes,
        sword,
        shield
    }
    public Sprite sprite;
    public string itemName;
    public string itemDesc;
    public int baseDurability;
    public float value;
    public ItemType itemType;
    public EffectType effectType;
    public EquipType armorType;
    public bool stackable;
}
