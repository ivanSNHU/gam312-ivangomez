﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideCollider : MonoBehaviour
{
    public bool somethingIsTouchingMe;

    // Start is called before the first frame update
    void Start()
    {
        somethingIsTouchingMe = false;
    }

    // Update is called once per frame
    void Update()
    {
        //this script lets you know if the collider is touching something
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        somethingIsTouchingMe = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        somethingIsTouchingMe = false;
    }
}
