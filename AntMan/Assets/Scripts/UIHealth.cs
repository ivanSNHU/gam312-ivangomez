﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHealth : MonoBehaviour
{
    Text myText;
    string defaultString;
    //GameObject player;
    StatsEffects playerStats;

    // Start is called before the first frame update
    void Start()
    {
        myText = GetComponent<Text>();
        defaultString = myText.text;
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<StatsEffects>();
    }

    // Update is called once per frame
    void Update()
    {
        myText.text = defaultString + playerStats.currentHealth + @"/" + playerStats.currentMaxHealth;
    }
}
