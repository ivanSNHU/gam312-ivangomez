﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript instance;
    BoxCollider2D bc2d;
    Rigidbody2D rb2d;
    JumpScript js;
    SideCollider[] sides;
    [SerializeField] int cooties = 0;
    [SerializeField] float walkForce = 500;
    [SerializeField] float jumpForce = 500;
    [SerializeField] float airForce = 250;
    [SerializeField] float xVelocityCap;
    [SerializeField] float yVelocityCap;
    [SerializeField] float shrinkSpeed;
    [SerializeField] Vector2 velocityReadout;
    [SerializeField] GameObject rotationHandle;
    [SerializeField] bool joystickControls = true;
    Animator animator;
    float zPosition;
    public string grow;
    public string shrink;
    public string jump;
    public string walk;
    public string horizontal;
    public string equip;
    public string discard;
    //AnimatorControllerParameter[] animatorControllerParameters;

    // Start is called before the first frame update
    void Start()
    {
        zPosition = transform.position.z;
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        bc2d = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        js = GetComponentInChildren<JumpScript>();
        sides = GetComponentsInChildren<SideCollider>();
        animator = rotationHandle.GetComponent<Animator>();
        //animatorControllerParameters = animator.parameters;
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        transform.position = new Vector3(transform.position.x, transform.position.y, zPosition);
        //if too many of my sides are touching something, up the cooties variable so we can't grow
        cooties = 0;
        foreach (var side in sides)
        {
            if (side.somethingIsTouchingMe)
            {
                cooties++;
            }
        }
        if (js.canJump)
        {
            cooties++;
        }

        if (joystickControls)
        {
            grow = "RB_1";
            shrink = "LB_1";
            jump = "A_1";
            walk = "L_XAxis_1";
            horizontal = "DPad_XAxis_1";
            equip = "X_1";
            discard = "B_1";
        }
        else
        {
            grow = "Fire1";
            shrink = "Fire2";
            jump = "Vertical";
            walk = "Horizontal";
            horizontal = "Horizontal";
            equip = "Submit";
            discard = "Cancel";
        }

        //grow on this key
        if (Input.GetButtonDown(grow) && transform.localScale.magnitude < 6.0f && cooties < 3)
        {
            transform.localScale = new Vector3(
                transform.localScale.x + shrinkSpeed,
                transform.localScale.y + shrinkSpeed,
                transform.localScale.z + shrinkSpeed);
        }
        //shrink on this key
        if (Input.GetButtonDown(shrink) && transform.localScale.magnitude > 0.5f)
        {
            transform.localScale = new Vector3(
                transform.localScale.x - shrinkSpeed,
                transform.localScale.y - shrinkSpeed,
                transform.localScale.z - shrinkSpeed);
        }
        //jump on this key
        if (Input.GetButtonDown(jump) && js.canJump)
        {
            rb2d.AddForce(Vector2.up * jumpForce);
        }
        //walk if we aren't going to fast
        if (rb2d.velocity.x < xVelocityCap && rb2d.velocity.x > -xVelocityCap && js.canJump)
        {
            rb2d.AddForce(new Vector2(Input.GetAxis(walk) * walkForce * dt, 0));
        }
        else //change velocity slower if we aren't touching the ground
        {
            rb2d.AddForce(new Vector2(Input.GetAxis(walk) * airForce * dt, 0));
        }

        velocityReadout = rb2d.velocity;
        if (velocityReadout.x > 0.1f)
        {
            rotationHandle.transform.eulerAngles = new Vector3(
                0,
                Mathf.LerpAngle(rotationHandle.transform.eulerAngles.y, 90, dt),
                0);
            SetRun();
            animator.SetFloat("RunBlend", Mathf.Min(rb2d.velocity.x, xVelocityCap) / xVelocityCap);
        }
        else if (velocityReadout.x < -0.1f)
        {
            rotationHandle.transform.eulerAngles = new Vector3(
                0,
                Mathf.LerpAngle(rotationHandle.transform.eulerAngles.y, 270, dt),
                0);
            SetRun();
            animator.SetFloat("RunBlend", Mathf.Min(-rb2d.velocity.x, xVelocityCap) / xVelocityCap);
        }
        else
        {
            rotationHandle.transform.eulerAngles = new Vector3(
                0,
                Mathf.LerpAngle(rotationHandle.transform.eulerAngles.y, 180, dt),
                0);
            SetIdle();
        }
    }

    private void FixedUpdate()
    {
        //limit x velocity and jump velocity and create drag
        if (js.canJump)
        {
            rb2d.velocity = new Vector2(
                Mathf.Clamp(rb2d.velocity.x, -xVelocityCap, xVelocityCap) * 0.9f,
                Mathf.Min(rb2d.velocity.y, yVelocityCap));
        }
        else
        {
            rb2d.velocity = new Vector2(
                Mathf.Clamp(rb2d.velocity.x, -xVelocityCap, xVelocityCap) * 0.9f,
                Mathf.Min(rb2d.velocity.y, yVelocityCap));
        }
    }

    private void SetIdle()
    {
        animator.ResetTrigger("Attack");
        animator.ResetTrigger("Jump");
        animator.ResetTrigger("Land");
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Fall");
        animator.ResetTrigger("TurnLeft");
        animator.ResetTrigger("TurnRight");
        animator.ResetTrigger("Idle");
    }

    private void SetRun()
    {
        //setIdle();
        animator.SetTrigger("Run");
    }
}