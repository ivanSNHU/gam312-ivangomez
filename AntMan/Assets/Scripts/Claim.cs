﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create claim dialog")]
public class Claim : ScriptableObject
{
    public List<ItemDef> rewards;
    public string text;
}
