﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    GameObject player;
    Rigidbody2D playerRigidbody;
    float zPosition;

    [SerializeField] float heightOffset;
    [SerializeField] float followStrength;
    [SerializeField] float xVelocityStrength;
    [SerializeField] float yVelocityStrength;
    [SerializeField] float xClamp;
    [SerializeField] float yClamp;
    //[SerializeField] float velocityMaxOffset;
    [SerializeField] Vector3 targetPosition;
    [SerializeField] Vector3 playerPosition;
    [SerializeField] Vector2 playerVelocity;

    // Start is called before the first frame update
    void Start()
    {
        //fetch my variables
        player = GameObject.FindGameObjectWithTag("Player");
        playerRigidbody = player.GetComponent<Rigidbody2D>();
        zPosition = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;

        playerPosition = player.transform.position;
        playerVelocity = playerRigidbody.velocity;

        //account for velocity to 'forward' shift camera
        targetPosition = new Vector3(
            playerPosition.x + (playerVelocity.x * xVelocityStrength),
            playerPosition.y + (playerVelocity.y * yVelocityStrength) + heightOffset,
            zPosition);

        //lerp toward that new target position
        transform.position = Vector3.Lerp(
            transform.position,
            targetPosition,
            dt * followStrength);

        //make sure the player is still on screen
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, playerPosition.x - xClamp, playerPosition.x + xClamp),
            Mathf.Clamp(transform.position.y, playerPosition.y + heightOffset - yClamp, playerPosition.y + heightOffset + yClamp),
            zPosition);
    }
}
