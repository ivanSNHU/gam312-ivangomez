﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory instance;
    public List<Item> spareItems = new List<Item>();
    public Dictionary<ItemDef.EquipType, Item> equipedItems = new Dictionary<ItemDef.EquipType, Item>();
    [SerializeField] float spares, equiped;
    [SerializeField] GameObject itemBodyPrefab;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        spares = spareItems.Count;
        equiped = equipedItems.Count;
        if(Input.GetButtonDown(PlayerScript.instance.equip))
        {
            equipItem(spareItems[CursorScript.instance.position]);
        }
        if(Input.GetButtonDown(PlayerScript.instance.discard))
        {
            DiscardItem(spareItems[CursorScript.instance.position]);
        }
    }

    void equipItem(Item item)
    {
        if (spareItems.Contains(item))
        {
            if (item.itemDef.itemType == ItemDef.ItemType.Consumable)
            {
                StatsEffects.instance.AddActiveEffect(new Effect(item.itemDef.effectType, item.itemDef.name, item.itemDef.value, false, 5.0f));
                item.quantity--;
                if (item.quantity <= 0)
                {
                    spareItems.Remove(item);
                }
            }            
            else if (equipedItems.ContainsKey(item.itemDef.armorType))
            {
                AddToSpareItems(equipedItems[item.itemDef.armorType]);
                equipedItems.Remove(item.itemDef.armorType);                
                equipedItems[item.itemDef.armorType] = item;
                spareItems.Remove(item);
            }            
            else
            {
                equipedItems[item.itemDef.armorType] = item;
                spareItems.Remove(item);
            }
        }
        //GameObject.Find("Panel").GetComponent<InventoryDisplay>().UpdateDisplay();
        InventoryDisplay.instance.UpdateDisplay();
    }

    void unequipItem(Item item)
    {
        if (equipedItems.ContainsValue(item))
        {
            Item newItem = item;
            AddToSpareItems(item);
            equipedItems.Remove(item.itemDef.armorType);
        }
        //GameObject.Find("Panel").GetComponent<InventoryDisplay>().UpdateDisplay();
        InventoryDisplay.instance.UpdateDisplay();
    }

    bool AddToSpareItems(Item item)
    {
        bool wasInInventory = false;
        if (item.pickedUp)
        {
            return false;
        }
        foreach (var spareItem in spareItems)
        {
            //if(spareItem != null)
            //{
            if (spareItem.itemDef == item.itemDef && item.itemDef.stackable)
            {
                spareItem.quantity++;
                InventoryDisplay.instance.UpdateDisplay();
                wasInInventory = true;
                break;
            }
            else if (spareItem.itemDef == item.itemDef && !item.itemDef.stackable)
            {
                spareItems.Add(item);
                InventoryDisplay.instance.UpdateDisplay();
                wasInInventory = true;
                break;
            }
            //}            
        }
        if (!wasInInventory)
        {
            spareItems.Add(item);
        }
        item.pickedUp = true;
        InventoryDisplay.instance.UpdateDisplay();
        return true;
    }

    void DiscardItem(Item item)
    {
        if (spareItems.Contains(item))
        {
            instantiateItem(item);
            spareItems.Remove(item);
        }
        else if (equipedItems.ContainsValue(item))
        {
            instantiateItem(item);
            equipedItems.Remove(item.itemDef.armorType);
        }
        //GameObject.Find("Inventory Display").GetComponent<InventoryDisplay>().UpdateDisplay();
        InventoryDisplay.instance.UpdateDisplay();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Item")
        {
            AddToSpareItems(collision.gameObject.GetComponent<ItemBody>().item);
            Destroy(collision.gameObject);
        }
    }

    private void instantiateItem(Item item)
    {
        GameObject newGameObject = Instantiate(itemBodyPrefab);
        newGameObject.transform.position = new Vector3(
            transform.position.x,
            transform.position.y + 1,
            transform.position.z);
        newGameObject.GetComponent<ItemBody>().item = item;
    }
}
