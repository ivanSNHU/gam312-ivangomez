﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public static ItemSpawner instance;
    [SerializeField] List<ItemDef> itemList = new List<ItemDef>();
    [SerializeField] GameObject itemBodyPrefab;
    [SerializeField] int spawnSpeed;
    float timeSinceSpawn;
    int currentUid = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        timeSinceSpawn += dt;
        if(timeSinceSpawn > spawnSpeed)
        {
            timeSinceSpawn = 0;
            GameObject newGameObject = Instantiate(itemBodyPrefab);
            newGameObject.transform.position = transform.position;
            newGameObject.GetComponent<ItemBody>().item = new Item(itemList[(int)(currentUid%itemList.Count)], currentUid);
            currentUid++;
        }
    }

    public void SpawnItem(float x, float y)
    {
        GameObject newGameObject = Instantiate(itemBodyPrefab);
        newGameObject.transform.position = new Vector2(x, y);
        newGameObject.GetComponent<ItemBody>().item = new Item(itemList[(int)(Time.deltaTime % itemList.Count)], currentUid);
        currentUid++;
    }
    public void SpawnItem(float x, float y, ItemDef itemDef)
    {
        GameObject newGameObject = Instantiate(itemBodyPrefab);
        newGameObject.transform.position = new Vector2(x, y);
        newGameObject.GetComponent<ItemBody>().item = new Item(itemDef, currentUid);
        currentUid++;
    }
}
