﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public float currentDurability;
    public ItemDef itemDef;
    public int quantity;
    public int uid;
    public bool pickedUp;

    public Item(ItemDef itemDef, int uid)
    {
        this.itemDef = itemDef;
        this.uid = uid;
        quantity = 1;
        currentDurability = this.itemDef.baseDurability;
        pickedUp = false;
    }
}
