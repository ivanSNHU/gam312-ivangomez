﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryDisplay : MonoBehaviour
{
    public static InventoryDisplay instance;
    GameObject player;
    [SerializeField] List<Image> images = new List<Image>();
    //List<Image> imageSlots = new List<Image>();
    //Inventory inventory;
    public int scrollValue = 0;
    //List<Sprite> sprites;
    //List<SpriteRenderer> spriteRenderers;
    [SerializeField] Vector2 startPoint;
    [SerializeField] int buffer;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateDisplay()
    {
        for (int i = 0; i < images.Count; i++)
        {
            try
            {
                images[i].sprite = Inventory.instance.spareItems[i].itemDef.sprite;
                images[i].enabled = true;
            }
            catch (Exception e)
            {
                images[i].enabled = false;
            }
        }
    }
}
