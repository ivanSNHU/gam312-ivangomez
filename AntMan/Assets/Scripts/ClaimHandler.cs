﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaimHandler : MonoBehaviour
{
    public Claim claim;
    public GameObject canvas;
    GameObject player;
    PlayerScript playerScript;

    // Start is called before the first frame update
    void Start()
    {
        //canvas = GetComponentInChildren<Canvas>();
        canvas.SetActive(false);
        player = GameObject.FindGameObjectWithTag("Player");
        playerScript = player.GetComponent<PlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        if(Vector2.Distance(player.transform.position, transform.position) < 3.0f)
        {
            canvas.SetActive(true);
            if (Input.GetButtonDown(playerScript.jump))
            {
                Claim();
            }
        }
        else
        {
            canvas.SetActive(false);
        }
    }

    void Claim()
    {
        foreach(ItemDef itemDef in claim.rewards)
        {
            ItemSpawner.instance.SpawnItem(transform.position.x, transform.position.y, itemDef);
        }
        Destroy(gameObject);
    }
}
