﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stat
{
    public ItemDef.EffectType statType;
    string effectName;
    float baseValue;
    public float value;
    public Stat(ItemDef.EffectType statType, string effectName, float baseValue, float value)
    {

    }
}

public class Stats
{
    public Dictionary<ItemDef.EffectType, Stat> stats = new Dictionary<ItemDef.EffectType, Stat>();
}

public class Effect
{
    public ItemDef.EffectType effectType;
    string effectName;
    public float value;
    public bool persistent;
    public float ttl;
    public Effect(ItemDef.EffectType effectType, string effectName, float value, bool persistent, float ttl)
    {
        this.effectType = effectType;
        this.effectName = effectName;
        this.value = value;
        this.persistent = persistent;
        this.ttl = ttl;
    }
}

public class StatsEffects : MonoBehaviour
{
    public static StatsEffects instance;
    public float currentHealth, currentMaxHealth, currentDamage, currentArmor;
    [SerializeField] float baseMaxHealth, baseDamage, baseArmor;
    List<Effect> activeEffects = new List<Effect>();
    Inventory inventory;
    float updateInterval = 3.0f;
    float timeSinceLastUpdate = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        timeSinceLastUpdate += dt;
        if (timeSinceLastUpdate > updateInterval)
        {
            Debug.Log("Gonna chech stats now");
            timeSinceLastUpdate -= updateInterval;
            float health = baseMaxHealth;
            float damage = baseDamage;
            float armor = baseArmor;
            RemovePersistentEffects();
            RemoveActiveEffects(dt);
            activeEffects.TrimExcess();
            TabulateEquipedStats();
            TabulateActiveEffects(dt, ref health, ref damage, ref armor);
            currentMaxHealth = health;
            currentDamage = damage;
            currentArmor = armor;
            currentHealth = Mathf.Min(currentHealth, currentMaxHealth);
        }
    }

    private void TabulateActiveEffects(float dt, ref float health, ref float damage, ref float armor)
    {
        foreach (var activeEffect in activeEffects)
        {
            if (activeEffect.effectType == ItemDef.EffectType.Armor)
            {
                armor += activeEffect.value;
            }
            else if (activeEffect.effectType == ItemDef.EffectType.Damage)
            {
                damage += activeEffect.value;
            }
            else if (activeEffect.effectType == ItemDef.EffectType.Healing)
            {
                currentHealth += activeEffect.value * dt;
            }
            else if (activeEffect.effectType == ItemDef.EffectType.MaxHealth)
            {
                health += activeEffect.value;
            }
        }
    }

    private void TabulateEquipedStats()
    {
        foreach (var item in inventory.equipedItems.Values)
        {
            Item nextItem = item;
            activeEffects.Add(new Effect(ItemDef.EffectType.Armor, "Armor", nextItem.itemDef.value, true, 0));
        }
    }

    private void RemoveActiveEffects(float dt)
    {
        for (int i = 0; i < activeEffects.Count;)
        {
            if (!activeEffects[i].persistent)
            {
                activeEffects[i].ttl -= dt;
                if (activeEffects[i].ttl < 0.0f)
                {
                    activeEffects.RemoveAt(i);
                }
                else
                {
                    ++i;
                }
            }
            else
            {
                ++i;
            }
        }
    }

    public void AddActiveEffect(Effect effect)
    {
        activeEffects.Add(effect);
    }

    private void RemovePersistentEffects()
    {
        for (int i = 0; i < activeEffects.Count;)
        {
            if (activeEffects[i].persistent)
            {
                activeEffects.RemoveAt(i);
            }
            else
            {
                ++i;
            }
        }
    }
}
