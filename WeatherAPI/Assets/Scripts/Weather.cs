﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;

public class Weather : MonoBehaviour
{
    string site = "http://api.openweathermap.org/data/2.5/weather?q=";
    public string location = "hooksett,nh,usa";
    string apiKey = "&appid=0d0112e68b8836e9a6139724e11b66c6";
    string url;
    bool daytime = true;
    float clearSkies = 100.0f;

    // Use this for initialization
    void Start()
    {
        url = site + location + apiKey;
        StartCoroutine(GetWeather());
    }

    IEnumerator GetWeather()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();

            // Your code here to get the text of the website
            string websiteText = webRequest.downloadHandler.text;

            SimpleJSON.JSONNode data = SimpleJSON.JSON.Parse(websiteText);
            if (data["sys"]["sunrise"] < data["dt"] && data["dt"] < data["sys"]["sunset"])
            {
                daytime = true;
            }
            else
            {
                daytime = false;
            }
            clearSkies = data["clouds"]["all"];
            UpdateDaylight();
            UpdateFog();

            Debug.Log(data.ToString());
        }
    }
    private void UpdateDaylight()
    {
        Light light = GameObject.FindGameObjectWithTag("Daylight").GetComponent<Light>();
        if (daytime)
        {
            Camera.main.backgroundColor = Color.white;
            light.color = Color.white;
        }
        else if (!daytime)
        {
            Camera.main.backgroundColor = Color.gray;
            light.color = Color.gray;
        }
    }

    private void UpdateFog()
    {
        RenderSettings.fog = true;
        RenderSettings.fogDensity = clearSkies / 10000.0f;
    }
}